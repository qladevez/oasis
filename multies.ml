(** Goal of this file
Program: several threads with read or write events from/to locations where a read event can potentially imply a branching in the execution. There is a total order on the events of each thread.

Event structure: several threads with read or write events from/to locations where no branching is involved. There is a total order on the events of each thread.

Execution witness: several threads with read or write events of values from/to locations where no branching is involved. There is a total order on the events of each thread. There is a read-from and write-serialisation relation coherent with the values read or written by events

Sequential execution: total order on all the events of a program, representing the actual order in which the events happen

The goal is to program several transformations

1. Transform a program in one or more event structures, by enumerating all the possible branching decisions
2. Transform an event structure in one or more execution witnesses, by enumerating all the possible read-from and write-serialisation relations and deducing the values from these relations
3. Transform an execution witness in one or more sequential executions, by total extension of program order, read-from, write serialisation and from-read
 *)

open Printf
open List

(** Memory addresses are integers *)
type addr = int

(** Value and Function ids are integers *)
type fid = int
type id = int

(** Set of ids *)
module IdComp =
  struct
    type t = id
    let compare = compare
  end

module IdSet = Set.Make(IdComp)
type id_set = IdSet.t

(** Locations are integers *)
type loc =
  Addr of addr
| DpAddr of fid * id_set

let is_addr = function
  | Addr _ -> true
  | _ -> false

let is_dp_addr = function
  | DpAddr _ -> true
  | _ -> false

let get_addr = function
  | Addr a -> a
  | _ -> failwith "Trying to get the address of a dp location"

module LocComp =
  struct
    type t = loc
    let compare = compare
  end

(** A set of events *)
module LocSet = Set.Make(LocComp)

type loc_set = LocSet.t
  
(** Values written are integers *)
type value = Value of int | Id of id

(** A thread program is either:

- A write event of a value to a location followed by the rest of the thread program
- A read event of a value from a location followed by a function that associates to the value read the rest of the program
- A branching between two thread programs
- The empty program
 *)
type thread =
  | Skip
  | Write of loc * value * thread
  | Read of loc * lambda
  | Branch of thread * thread
and lambda = Abstr of id * thread

(** A program is a list of thread programs *)
type prog = thread list

(** An action is either:

- A write of a value to a location
- A read of a value from a location *)
type action =
  | EWrite of loc * value
  | ERead of loc * value

(** An event identifier is an integer *)
type eid = int

(** An event is an action and an event identifier *)
type event = Event of eid * action

let is_write e =
  match e with
  | Event (_, EWrite _) -> true
  | _ -> false

let is_read e =
  match e with
  | Event (_, ERead _) -> true
  | _ -> false

let get_loc e =
  match e with
  | Event (_, EWrite (loc,_)) -> loc
  | Event (_, ERead (loc,_)) -> loc

let get_value e =
  match e with
  | Event (_, EWrite (_,value)) -> value
  | Event (_, ERead (_,value)) -> value

(** Comparing events

Events are compared based on their event identifiers *)
module EvtComp =
  struct
    type t = event
    let compare e1 e2 =
      match (e1,e2) with
      | (Event (i1,_), Event (i2,_)) -> compare i1 i2
  end

(** A set of events *)
module ESet = Set.Make(EvtComp)

type ev_set = ESet.t
            
(** Comparing pairs of events

Pairs of events are compared based on the lexicographic orders of their event identifiers *)
module EvtPairComp =
  struct
    type t = event * event
    let compare (e1,e2) (e3,e4) =
      match (e1,e2,e3,e4) with
      | (Event (i1,_), Event (i2,_), Event (i3,_), Event (i4,_)) ->
         match compare i1 i3 with
           0 -> compare i2 i4
         | c -> c
  end

(** A relation on events is a set of pairs of events *)
module EPSet = Set.Make(EvtPairComp)

type ev_rln = EPSet.t

(** An event structure is composed of:

- A set of events
- A program order relation on events *)
type event_structure = {
    evts: ev_set;
    po: ev_rln
  }

(** All the write events from an event structure *)
let write_events es = ESet.filter is_write es.evts

(** All the read events from an event structure *)
let read_events es = ESet.filter is_read es.evts

(** Set of locations involved in an event structure *)
let es_locations_as_set es =
  let evt_list = ESet.elements es.evts in
  let loc_list = map get_loc evt_list in
  LocSet.of_list loc_list
  
(* It may look like to construct the list of locations, we build a list, turn it into a set and into a list again. This is done on purpose to avoid duplicates in the list *)
(** List of the locations involved in an event structure *)
let es_locations_as_list es =
  LocSet.elements (es_locations_as_set es)

let es_addr_locations_as_list es =
  filter is_addr (es_locations_as_list es)

(** Set of addresses involved in an event structure *)
let es_addrs_as_set es =
  LocSet.filter is_addr (es_locations_as_set es)

(** List of addresses involved in an event structure *)
let es_addrs_as_list es =
  filter is_addr (es_locations_as_list es) |>
  map get_addr

(** Set of the locations involved in a program *)
let rec thread_locations t =
  match t with
  | Skip -> LocSet.empty
  | Write (l, _, k) -> LocSet.add l (thread_locations k)
  | Read (l, Abstr (_,k)) -> LocSet.add l (thread_locations k)
  | Branch (k1, k2) -> LocSet.union (thread_locations k1) (thread_locations k2)

let rec prog_locations p =
  match p with
  | [] -> LocSet.empty
  | (t::ts) -> LocSet.union (thread_locations t) (prog_locations ts)

let prog_locations_as_list p =
  LocSet.elements (prog_locations p)
                               
(** All the write events to a given location *)
let write_events_for_location es loc =
  ESet.filter (fun e -> (get_loc e) = loc) (write_events es)

(** [rel_of_evts_list e l] returns the set containing pairs (e, l[0]), (e, l[1]), etc... for each element of l *)
let rec rel_of_evts_list e = function
  | [] -> EPSet.empty
  | e'::l -> EPSet.add (e,e') (rel_of_evts_list e l)

(** [rel_of_evts_list_rev e l] returns the set containing the pairs (l[0], e), (l[1], e), etc ... for each element of l *)
let rec rel_of_evts_list_rev e = function
  | [] -> EPSet.empty
  | e'::l -> EPSet.add (e',e) (rel_of_evts_list_rev e l)

(** [evt_list_to_rel l] returns a relation such that an element [x] is in relation with [y] if [x] appears before [y] in [l] *)
let rec evt_list_to_rel = function
  | [] -> EPSet.empty
  | (e::l) -> EPSet.union (rel_of_evts_list e l) (evt_list_to_rel l)
  
(** The list of all the possible pairs of values from two list fed to a function *)
let dot_prod_with f l1 l2 =
  flatten (map (fun x -> map (f x) l2) l1)

(** [zip_with f [x1;...;xn] [y1;...;yn]] produces [f x1 y1; f x2 y2; ...; f xn yn] *)
let rec zip_with f l1 l2 =
  match (l1,l2) with
  | (_,[]) -> []
  | ([],_) -> []
  | (x::xs, y::ys) -> (f x y)::(zip_with f xs ys)

(** List of integers from 0 to n-1 *)
let list_nums_to_n n =
  let rec aux k =
    if k == n
    then []
    else k::(aux (k+1))
  in
  aux 0

(** Number a list *)
let number_list l =
  zip_with (fun x y -> (x,y)) (list_nums_to_n (length l)) l
            
let enum_branchings p =
  let rec aux p fresh_eid = 
    let rec enum_for_thread t fresh_eid =
      match t with
      | Skip -> (fresh_eid, [{ evts = ESet.empty; po = EPSet.empty }])
      | Write (l, v, k) -> let (new_fresh_eid, t') = enum_for_thread k (fresh_eid+1) in
                           let evt = Event (fresh_eid, EWrite (l,v)) in
                           (new_fresh_eid,
                            map (fun es -> 
                                { evts = ESet.add evt es.evts;
                                  po = EPSet.union es.po (rel_of_evts_list evt (ESet.elements es.evts)) }) t')
      | Read (l, Abstr (id, k)) -> let (new_fresh_eid, t') = enum_for_thread k (fresh_eid+1) in
                                   let evt = Event (fresh_eid, ERead (l, Id id)) in
                                   (new_fresh_eid,
                                    map (fun es ->
                                        { evts = ESet.add evt es.evts;
                                          po = EPSet.union es.po (rel_of_evts_list evt (ESet.elements es.evts)) }) t')
      | Branch (k, k') -> let (new_fresh_eid, lbranch) = enum_for_thread k fresh_eid in
                          let (new_fresh_eid', rbranch) = enum_for_thread k' fresh_eid in
                          (max new_fresh_eid new_fresh_eid', lbranch @ rbranch)
    in 
    match p with
    | [] -> (fresh_eid, [{ evts = ESet.empty; po = EPSet.empty }])
    | t::ts -> 
       let (new_fresh_eid, es) = enum_for_thread t fresh_eid in
       let (new_fresh_eid', es') = aux ts new_fresh_eid in
       (new_fresh_eid', dot_prod_with (fun x y -> { evts = ESet.union x.evts y.evts; po = EPSet.union x.po y.po }) es es')
  in
  let (_, result) = aux p (length (prog_locations_as_list p)) in result
       
(** An execution witness is composed of

- A read-from relation on events
- A coherence-order relation on events
 *)
type exec_witness = {
    rf: ev_rln;
    co: ev_rln
  }

(** Generate execution witnesses from event structures *)

(** Insert an element in all possible positions in a list *)
let ins_all_positions x l =  
  let rec aux prev acc = function
    | [] -> (prev @ [x]) :: acc |> rev
    | hd::tl as l -> aux (prev @ [hd]) ((prev @ [x] @ l) :: acc) tl
  in
  aux [] [] l

(** All possible permutations of a list *)
let rec permutations = function  
  | [] -> []
  | x::[] -> [[x]]
  | x::xs -> fold_left (fun acc p -> acc @ ins_all_positions x p ) [] (permutations xs)                

(** Write serialisations for the writes to a specific location *)
let write_serialisation_by_loc es loc =
  let write_evts_list = ESet.elements (write_events_for_location es loc) in
  let write_evts_permutations = permutations write_evts_list in
  let res = map evt_list_to_rel write_evts_permutations in
  if res == []
  then [EPSet.empty]
  else res

(** All possible write serialisations of an event structure *)
let write_serialisation es =
  let rec aux = function
    | [] -> [EPSet.empty]
    | (l::loc) -> dot_prod_with EPSet.union (write_serialisation_by_loc es l) (aux loc)
  in
  aux (es_addr_locations_as_list es)

let read_from_by_read es r =
  let loc = get_loc r in
  let writes_to_loc = ESet.elements (write_events_for_location es loc) in
  let locs_list = es_locations_as_list es in
  let rec find_default_write_eid locations acc =
    match locations with
    | [] -> 0
    | (l::ls) -> if (l = loc) then acc else (find_default_write_eid ls (acc+1)) in
  let default_write_eid = find_default_write_eid locs_list 0 in
  let rec aux = function
    | [] ->
       if is_addr loc
       then [EPSet.add (Event (default_write_eid, EWrite (loc, Value 0)), r) EPSet.empty]
       else []
    | (w::ws) -> (EPSet.add (w,r) EPSet.empty)::(aux ws) in
  aux writes_to_loc
  
  
(** All possible read-from relations of an event structure *)
let read_from es =
  let rec aux = function
    | [] -> [EPSet.empty]
    | (r::rs) -> dot_prod_with EPSet.union (read_from_by_read es r) (aux rs)
  in
  aux (ESet.elements (read_events es))

(** Pretty-printing an execution structure thread 

We represent memory locations and lambda expression parameters with integers, which is a little hard to read. The following functions allow the printing of an execution structure in a clearer way
*)

(** The letters we want to use to name memory locations and parameters of lambda expressions *)
let loc_names = ["x"; "y"; "z"; "w"]
(* let binding_names = ["k"; "l"; "m"; "p"] *)
let binding_names = ["k"]
let function_names = ["f"; "g"; "h"]
                    
(** Generate a string consisting of n lambdas *)
let n_lambdas n = String.concat "" (init n (fun _ -> "'"))

(** Associate to each natural number a distinct identifier composed of the list of identifiers in [id_list] and of a certain number of lambdas *)
let generate_id n id_list =
  let names_length = length id_list in
  String.concat "" [
      nth id_list (n mod names_length);
      n_lambdas (n / names_length)]

(** String representation of an address *)
let pp_addr a = generate_id a loc_names
  
(** String representation of a location *)
let pp_loc l =
  match l with
  | Addr a -> pp_addr a
  | DpAddr (fid, ids) -> String.concat "" [
                             generate_id fid function_names;
                             "(";
                             String.concat "," (map (fun i -> generate_id i binding_names) (IdSet.elements ids));
                             ")"
                           ]

(** String representation of a lambda expression parameter *)
let pp_binding b = generate_id b binding_names

(** String representation of a value (either an integer or a name) *)
let pp_value v =
  match v with
  | Value v -> Int.to_string v
  | Id i -> pp_binding i
          
(** String representation of an action *)
let pp_action a =
  match a with
  | EWrite (l, v) -> String.concat "" ["W"; pp_loc l; pp_value v]
  | ERead (l, v) -> String.concat "" ["R"; pp_loc l; pp_value v]
                  
(** DOT format representation of events, relations and more generally event structures *)
let dot_es_evt e =
  match e with
  | Event (id, action) -> String.concat "" [Int.to_string id; " [label=\""; pp_action action; "\"]"]

let dot_es_rel label color (Event (s,_) , Event (d,_)) =
  match (label, color) with
  | (None,None) -> String.concat " " [Int.to_string s; "->"; Int.to_string d]
  | (Some label,None) -> String.concat " " [Int.to_string s; "->"; Int.to_string d; "[label="; label;"]"]
  | (None, Some color) -> String.concat " " [Int.to_string s; "->"; Int.to_string d; "[color="; color;"]"]
  | (Some label, Some color) -> String.concat " " [Int.to_string s; "->"; Int.to_string d; "[color="; color; "label="; label; "]"]

let dot_es_init l =
  String.concat "" [Int.to_string l; " [label=\"IW"; pp_addr l; "0\"]"]
                        
let dot_event_structure_inits es =
  String.concat ";\n" (map dot_es_init (es_addrs_as_list es))
  
let dot_event_structure_evts es =
  String.concat ";\n" (map dot_es_evt (ESet.elements es.evts))

let dot_event_structure_po es =
  String.concat ";\n" (map (dot_es_rel (Some "po") None) (EPSet.elements es.po))

(** Writing an event structure in DOT format in a file *)
let write_dotfile_for_es name es =
  let oc = open_out name in
  fprintf oc "digraph ES {\n%s\n%s\n}" (dot_event_structure_evts es) (dot_event_structure_po es);
  close_out oc

let dot_write_serialisation ws =
  String.concat ";\n" (map (dot_es_rel (Some "ws") (Some "blue")) (EPSet.elements ws))

let dot_read_from rf =
  String.concat ";\n" (map (dot_es_rel (Some "rf") (Some "red")) (EPSet.elements rf))
                     
let write_dotfile_for_ew name es ws rf =
  let oc = open_out name in
  (match (ws, rf) with
   | (None, None) -> fprintf oc "digraph ES {\n%s\n%s\n%s\n}" (dot_event_structure_inits es) (dot_event_structure_evts es) (dot_event_structure_po es) 
   | (Some ws, None) -> fprintf oc "digraph ES {\n%s\n%s\n%s\n%s\n}" (dot_event_structure_inits es) (dot_event_structure_evts es) (dot_event_structure_po es) (dot_write_serialisation ws)
   | (None, Some rf) -> fprintf oc "digraph ES {\n%s\n%s\n%s\n%s\n}" (dot_event_structure_inits es) (dot_event_structure_evts es) (dot_event_structure_po es) (dot_read_from rf)
   | (Some ws, Some rf) -> fprintf oc "digraph ES {\n%s\n%s\n%s\n%s\n%s\n}" (dot_event_structure_inits es) (dot_event_structure_evts es) (dot_event_structure_po es) (dot_write_serialisation ws) (dot_read_from rf));
  close_out oc

(** Writing a list of event structures in different files *)
let write_dotfiles_for_es_list es =
  let rec aux i es =
    match es with
    | [] -> ()
    | (e::es) ->
       write_dotfile_for_es (String.concat "" ["es"; Int.to_string i; ".dot"]) e;
       aux (i+1) es
  in
  aux 0 es

let some = fun x -> Some x
                    
let write_dotfiles_for_ew es =
  map (fun e -> (e, write_serialisation e)) es |> 
  map (fun (e,ws) -> (e, if ws == [] then [None] else map some ws)) |>
  map (fun (e,ws) -> map (fun w -> (e,w)) ws) |> flatten |>
  map (fun (e,w) -> ((e, w), read_from e)) |>
  map (fun ((e,w),rf) -> ((e, w), if rf == [] then [None] else map some rf)) |>
  map (fun ((e,w),rf) -> map (fun r -> (e,w,r)) rf) |> flatten |> number_list |>
  map (fun (n, (e,w,r)) -> (n,e,w,r)) |>
  map (fun (n, e,w,r) -> write_dotfile_for_ew (String.concat "" [Int.to_string n; ".dot"]) e w r)

let clean_then_write es =
  let _ = Sys.command "rm *.dot" in
  write_dotfiles_for_ew es

(** Example values used for testing *)
let ex_write = EWrite (Addr 0, Value 0)
let ex_evt_list = [Event (0, ex_write); Event (1, ex_write); Event (2, ex_write); Event (3, ex_write)]

(** A quick test of enum_branchings

T0:         | T1:
a <- x      | b <- y
if (test a) | if (test b)
then y <- a | then x <- b
else z <- a | else z <- b

Locations:
x = 0
y = 1
z = 2 
 *)
let test_thread0 = Read (Addr 0,
                        Abstr (0,
                               Branch (Write (Addr 1, (Id 0), Skip),
                                       Write (Addr 2, (Id 0), Skip))))

let test_thread1 = Read (Addr 1,
                        Abstr (1,
                               Branch (Write (Addr 0, (Id 1), Skip),
                                       Write (Addr 2, (Id 1), Skip))))

let test_program = [test_thread0; test_thread1]
let test_result = enum_branchings test_program

(**
x <- 1 | y <- 1
a <- y | b <- x

Locations:
x = 0
y = 1
 *)
                
let t3_t0 = Write (Addr 0, (Value 1), Read (Addr 1, Abstr (0, Skip)))
let t3_t1 = Write (Addr 1, (Value 1), Read (Addr 0, Abstr (1, Skip)))
let t3_prog = [t3_t0; t3_t1]
let t3_res = enum_branchings t3_prog

let _ =
  clean_then_write test_result
