(** Goal of this file

I want to enumerate all the executions that display a cycle
in (po U rf U mo U rb), i.e. a non-SC execution.
To control the size of the space, we bound the size of the
cycle.
To avoid non-sensical results, we want to filter executions
that are incoherent w.r.t to the nature of the relations.

The idea, is to generate a cycle of n relations, and then
to:

1. filter the non-sensical cycles
2. generate events thare compatible with these relations *)

open List
open Printf

type loc = int
type value = int

type evt = Write of loc * value | Read of loc * value

type rlt = Po | Ppo | Rf | Mo | Rb

let is_a_po r =
	match r with
	| Po -> true
	| Ppo -> true
	| _ -> false

let non_a_po r =
	not (is_a_po r)

(* What relation is possible before a given relation *)

let prev_rlt r =
	match r with
	| Po  -> [Ppo;Rf;Mo;Rb]
	| Ppo -> [Po;Rf;Mo;Rb]
	| Rf  -> [Po;Ppo;Mo;Rb]
	| Mo  -> [Po;Ppo]
	| Rb  -> [Po;Ppo;Rf]

(* generate the set of coherent sequences of n steps of
relations *)

exception PredsOfEmpty

let preds seq =
	match seq with
	| [] -> raise PredsOfEmpty
	| (r::rs) -> map (fun x -> x::seq) (prev_rlt r)

	let rec gen_seqs n =
	if n = 0 
	then []
	else
		if n = 1 
		then [[Po];[Rf];[Mo];[Rb]]
		else flatten (map preds (gen_seqs (n-1)))

(* generate coherent cycles of size n by filtering sequences
of relations *)

exception LastOfEmpty

let rec last xs =
	match xs with
	| [] -> raise LastOfEmpty
	| x::[] -> x
	| y::ys -> last ys

let rec is_in x xs =
	match xs with
	| [] -> false
	| y::ys -> if x = y then true else (is_in x ys)

let is_coh_seq seq = is_in (last seq) (prev_rlt (hd seq))

let gen_cycles n =
	filter is_coh_seq (gen_seqs n)

(* We want to filter executions who have a cycle in program
order *)

let has_cycle_in_po seq =
	for_all is_a_po seq

let non_has_cycle_in_po seq =
	not (has_cycle_in_po seq)

let filter_po_cycles seqs =
	filter non_has_cycle_in_po seqs

(* Once we have generated cycles, we'd like to remove those
that are identical modulo rotation *)

(* perform one (left) rotation *)
let rotate seq =
	match seq with
	| [] -> []
	| x::xs -> append xs [x]

(* performe n (left) rotations *)
let rec n_rotate seq n =
	if n = 0 then seq else rotate (n_rotate seq (n-1))

(* enumerate all the rotations of a list *)
let rec rots_aux seq n =
	if n = 0 then [seq] else (n_rotate seq n)::(rots_aux seq (n-1))

let rots seq =
	rots_aux seq (length seq - 1)

(* filter out rotations of a list of seqs *)

(* is one of the element of a list in another list *)

let rec is_one_in xs ys =
	match xs with
	| [] -> false
	| (z::zs) -> if is_in z ys then true else is_one_in zs ys

let rec filter_rots seqs =
	match seqs with
	| [] -> []
	| (s::rest) -> 
		if (is_one_in (rots s) rest) 
		then filter_rots rest
		else s::(filter_rots rest)

(* You need more than one po relation in your cycle.
If you have one po or less, it means that the cycle takes
place in a po_loc U com cycle, which is forbidden by all
models.
We filter out cycles with one po or less *)

(* count the number of relations that match a predicate in
a sequence *)

let rec count_is f xs =
	match xs with
	| [] -> 0
	| (z::zs) -> 
		if f z
		then 1 + (count_is f zs)
		else count_is f zs

(* We count the number of subsequences in sequence who contain
only relations that match a predicate *)

let rec count_subseq_is_aux acc in_subseq f xs =
	match xs with
	| [] -> acc
	| (z::zs) ->
		if (f z)
		then
			if in_subseq
			then count_subseq_is_aux acc true f zs
			else count_subseq_is_aux (acc+1) true f zs
		else
			count_subseq_is_aux acc false f zs

let count_subseq_is f xs =
	count_subseq_is_aux 0 false f xs

let more_than_one_po_seq xs =
	count_subseq_is is_a_po xs > 1

let filter_sc_by_loc xs =
	filter more_than_one_po_seq xs

(* If the cycle is a cycle in (ppo U rf), it is an OOTA
execution, and we can discard it *)

let is_oota seq =
	count_is (fun x -> x = Ppo || x = Rf) seq == (length seq)

let is_non_oota seq =
	not (is_oota seq)

let filter_oota seqs =
	filter is_non_oota seqs

(* If we imagine that all accesses are RA, we can consider
that that a race between two events related by (po U rf)+
in either direction is actually not race.
So this is filter we can apply on a list of cycles that
rules out such non-races.
For all events of the cycle to be related by (po U rf), 
theremust be at most one relation in the cycle that is
neither po or rf. *)

(*
let non_race_ra xs =
	(count_rel Po xs) + (count_rel Rf xs) < (List.length xs) - 1

let race_ra xs =
	not (non_race_ra xs)

let filter_ra_nonraces xs =
	filter race_ra xs
*)

(* We could detect the executions where it is possible to break
the cycle, without removing the race.
A cycle of size n can be broken without removing the race,
if we can find a rotation of the cycle that starts with mo
or rb, such that if we remove this first mo or rb step, the
execution still fails the non_race_ra test.
If the rotation started with sb or rf, removing the first
would mean removing a write without removing the reads that
depend of it, or removing an event without removing events
that follow it in program order.
*)

(* rotations that start with a mo or a rb *)

(*
let start_with_morb xs =
	match xs with
	| [] -> false
	| (y::ys) -> (y = Mo || y = Rb) 

let rotations_start_morb xs =
	filter start_with_morb (rots xs)

let race_breakable xs =
	exists race_ra (map List.tl (rotations_start_morb xs))

let filter_race_breakable xs =
	filter (fun x -> not (race_breakable x)) xs 
*)

(* printer functions *)

let p_rlt r =
	match r with
	| Po -> "po"
	| Ppo -> "ppo"
	| Rf -> "rf"
	| Mo -> "mo"
	| Rb -> "rb"

let p_seq seq =
	String.concat " ; " (map p_rlt seq)

let p_list_seqs seqs =
	(Int.to_string (length seqs)) ^ " sequences :\n\n" ^
	String.concat "\n" (map p_seq seqs)

let p_size_n n =
	print_string "\n=== Cycle of size ";
	print_int n;
	print_string " ===\n";
	gen_cycles n |>
	filter_po_cycles |>
	filter_oota |>
	filter_rots |>
	filter_sc_by_loc |>
	(* (* comment the following line if you don't want to filter out 
	races that are benign due to accesses being RA *)
	filter_ra_nonraces |>
	filter_race_breakable |> *)
	p_list_seqs |>
	print_string;
	print_string "\n"


let s_cyc_min = 3
let s_cyc_max = 5

(* Print cycles whose size is between two bounds *)

let rec p_from_to min max =
	p_size_n min;
	if min < max then p_from_to (min+1) max else ()

let _ =
	print_string "\n=== BEGIN RESULTS ===\n\n";
	p_from_to s_cyc_min s_cyc_max;
	print_string "\n\n=== END RESULTS ===\n\n"